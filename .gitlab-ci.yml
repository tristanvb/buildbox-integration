variables:
  BST: "bst --log-file logs/build.log --colors"

  # Docker image for running BuildStream to build the images
  BST_IMAGE_NAME: "registry.gitlab.com/freedesktop-sdk/infrastructure/freedesktop-sdk-docker-images"
  BST_IMAGE_ID: "543571583e6271ede4c706aff60d389cf26a13e9"

  # Docker image for testing BuildStream 2 against the built binaries
  TEST_IMAGE: registry.gitlab.com/buildstream/buildstream-docker-images/testsuite-fedora:36-master-533491591
  TEST_BST_REF: 1.95.0

  # Docker image containing debian base system
  DEBIAN_IMAGE_NAME: "registry.gitlab.com/buildgrid/buildbox/buildbox-docker-images/artifact-cache"
  DEBIAN_IMAGE_TAG: "master-613509386"

  # Package names, PKG_TRIPLE can be controlled by different jobs producing binaries for different platforms
  PKG_TRIPLE: "x86_64-linux-gnu"
  PKG_BIN_PREFIX: "buildbox"
  PKG_DBG_PREFIX: "buildbox-debug"

stages:
- prepare
- build
- test
- upload
- release


#####################################################
#     Prepare stage, workaround gitlab issue        #
#####################################################
#
# Using the `docker` source from bst-external hits some ugly permission
# errors when attempting to download the actual blobs from the gitlab docker
# registry.
#
# Workaround this mess with brute force.
#
prepare:
  stage: prepare
  image: docker:latest

  services:
    - docker:dind

  script:
    - ./get-docker-image.sh --image "${DEBIAN_IMAGE_NAME}" --tag "${DEBIAN_IMAGE_TAG}" --output tarball/debian.tar
  artifacts:
    paths:
    - tarball


#####################################################
#                   Track and build                 #
#####################################################

#
# Track and build everything, using the tarball we obtained
#
# Perform a sed job to control whether we want to build released versions
# or build master.
#
.build-template:
  image: "${BST_IMAGE_NAME}/bst2:${BST_IMAGE_ID}"
  stage: build
  needs:
  - job: prepare
    artifacts: true
  script:
  - mkdir -p logs
  - mkdir -p release
  - ${BST} --option stable ${BST_STABLE} source track --deps all buildbox.bst
  - ${BST} build buildbox.bst
  - ${BST} artifact checkout --directory binaries buildbox.bst
  - tar -zcf "release/${PKG_BIN_PREFIX}-${PKG_TRIPLE}.tgz" -C binaries/usr/bin .
  - tar -zcf "release/${PKG_DBG_PREFIX}-${PKG_TRIPLE}.tgz" -C binaries/usr/lib/debug .
  artifacts:
    when: always
    paths:
    - logs
    - release

#
# Build stable binaries
#
build-stable:
  extends:
  - .build-template
  variables:
    BST_STABLE: "True"
  except:
  - pipelines

#
# Build master binaries when triggered by buildbox component pipelines
#
build-master:
  extends:
  - .build-template
  variables:
    BST_STABLE: "False"
  only:
  - pipelines


#####################################################
#               Test the built binaries             #
#####################################################

#
# Here we an image from buildstream-docker-images to test
# BuildStream 2 in order to verify that the integration tests
# (which leverage the sandbox) succeed when ran against the
# freshly built static buildbox binaries.
#
.test-template:
  stage: test
  image: "${TEST_IMAGE}"
  script:
  # Unpack buildbox
  - mkdir buildbox
  - tar -C buildbox -zxf "release/${PKG_BIN_PREFIX}-${PKG_TRIPLE}.tgz"
  - export PATH="${CI_PROJECT_DIR}/buildbox:${PATH}"
  # Obtain BuildStream for testing
  - git clone https://github.com/apache/buildstream.git
  - cd buildstream
  - git checkout "${TEST_BST_REF}"
  # Run integration tests
  - tox -vvvvv -- --color=yes --integration tests/integration

#
# Test stable binaries
#
test-stable:
  extends:
  - .test-template
  needs:
  - job: build-stable
    artifacts: true
  except:
  - pipelines

#
# Test master binaries when triggered by buildbox component pipelines
#
test-master:
  extends:
  - .test-template
  needs:
  - job: build-master
    artifacts: true
  only:
  - pipelines


#####################################################
#                  Release mechanics                #
#####################################################

#
# Upload release assets in preparation for the release
#
upload:
  stage: upload
  image: curlimages/curl:latest
  needs:
  - job: build-stable
    artifacts: true
  - job: test-stable
  rules:
    - if: $CI_COMMIT_TAG                  # Run this job when a tag is created manually
  script:
    - |
      curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" \
           --upload-file "release/${PKG_BIN_PREFIX}-${PKG_TRIPLE}.tgz" \
           "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/releases/${CI_COMMIT_TAG}/${PKG_BIN_PREFIX}-${CI_COMMIT_TAG}-${PKG_TRIPLE}.tgz"
    - |
      curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" \
           --upload-file "release/${PKG_DBG_PREFIX}-${PKG_TRIPLE}.tgz" \
           "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/releases/${CI_COMMIT_TAG}/${PKG_DBG_PREFIX}-${CI_COMMIT_TAG}-${PKG_TRIPLE}.tgz"

#
# Create the gitlab release
#
release:
  stage: release
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  needs:
  - job: upload
  rules:
    - if: $CI_COMMIT_TAG                  # Run this job when a tag is created manually
  script:
    - echo "Welcome to the release job"
  release:
    tag_name: $CI_COMMIT_TAG
    name: 'Release $CI_COMMIT_TAG'
    description: 'Release created using the release-cli.'
    assets:
      links:
      - name: "${PKG_BIN_PREFIX}-${CI_COMMIT_TAG}-${PKG_TRIPLE}.tgz"
        url: "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/releases/${CI_COMMIT_TAG}/${PKG_BIN_PREFIX}-${CI_COMMIT_TAG}-${PKG_TRIPLE}.tgz"
        filepath: "/${PKG_BIN_PREFIX}-${PKG_TRIPLE}.tgz"
        link_type: other
      - name: "${PKG_DBG_PREFIX}-${CI_COMMIT_TAG}-${PKG_TRIPLE}.tgz"
        url: "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/releases/${CI_COMMIT_TAG}/${PKG_DBG_PREFIX}-${CI_COMMIT_TAG}-${PKG_TRIPLE}.tgz"
        filepath: "/${PKG_DBG_PREFIX}-${PKG_TRIPLE}.tgz"
        link_type: other
