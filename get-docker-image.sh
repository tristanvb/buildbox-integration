#!/bin/sh

usage () {
    echo "Usage: "
    echo "  get-docker-image.sh [OPTIONS]"
    echo
    echo "Downloads the base docker image for use as a tarball"
    echo
    echo "Options:"
    echo
    echo "  -h --help                      Display this help message and exit"
    echo "  -o --output   <filename>       The output filename for the tarball"
    echo "  -i --image    <image name>     The name of the image to fetch"
    echo "  -t --tag      <image tag>      The tag of the image"
    echo "  "
}

arg_output=
arg_image="registry.gitlab.com/buildgrid/buildbox/buildbox-docker-images/artifact-cache"
arg_tag="latest"

while : ; do
    case "$1" in
	-h|--help)
	    usage;
	    exit 0;
	    shift ;;

	-o|--output)
	    arg_output=${2};
	    shift 2 ;;

	-i|--image)
	    arg_image=${2};
	    shift 2 ;;

	-t|--tag)
	    arg_tag=${2};
	    shift 2 ;;

	*)
	    break ;;
    esac
done

docker --version >/dev/null
if test "$?" -ne "0"; then
    echo "docker is not installed"
    exit 1
fi

if test -z "${arg_output}"; then
    echo "Must specify output file"
    echo
    usage
    exit 1
fi

if test -f "${arg_output}"; then
    echo "The output file: ${arg_output} already exits"
    echo
    usage
    exit 1
fi

pullImage() {
    echo "Pulling image: ${arg_image}:${arg_tag}"
    docker pull "${arg_image}:${arg_tag}"
    if test "$?" -ne "0"; then
	echo "Failed to download docker image: ${arg_image}:${arg_tag}"
	exit 1
    fi
}

extractImage() {
    echo "Extracting image: ${arg_image}:${arg_tag}"
    container="$(docker container create ${arg_image}:${arg_tag})"
    if [ "$?" -ne "0" ]; then
	echo "Failed to create container for image: ${arg_image}:${arg_tag}"
	exit 1
    fi

    mkdir -p "$(dirname ${arg_output})"

    docker export "${container}" > "${arg_output}"
    if test "$?" -ne "0"; then
	echo "Failed to export container for image: ${arg_image}:${arg_tag}"
	exit 1
    fi

    docker rm "${container}"
    if test "$?" -ne "0"; then
	echo "WARNING: Failed to cleanup container: ${container}"
    fi
}

pullImage
extractImage
