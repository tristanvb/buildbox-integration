What is this for ?
==================
This repository contains a BuildStream project and some gitlab CI automation for
the purpose of automating the release of static buildbox binaries.


How do we maintain it ?
-----------------------
Whenever buildbox binaries have been tagged with a new release, this repository
should have a tag created, which will trigger the release mechanics to build
and publish new buildbox static binaries.

If the base debian image we use to build on top of has changed, then the ``.gitlab-ci.yml``
file should be updated to reflect this.


How to build locally ?
----------------------

* Observe the ``DEBIAN_IMAGE_NAME`` and ``DEBIAN_IMAGE_TAG`` in the ``.gitlab-ci.yml``

* Obtain the docker image as a tarball:

  .. code:: shell

     ./get-docker-image.sh --image "${DEBIAN_IMAGE_NAME}" --tag "${DEBIAN_IMAGE_TAG}" --output "tarball/debian.tar"

* Track and build the project:

  .. code:: shell

     bst source track --deps all buildbox.bst
     bst build buildbox.bst

* Obtain the output simply by checking it out:

  .. code:: shell

     bst artifact checkout --directory output buildbox.bst
